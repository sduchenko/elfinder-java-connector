package ef.impl.volume;

/**
 * User: sduchenko
 * Date: 18.04.13
 * Time: 18:22
 */
public class FsConfig {
    private final String rootPath;
    private final String volumeId;
    private final String defaultTextFileEncoding;

    public FsConfig(String rootPath, String volumeId, String defaultTextFileEncoding) {
        this.rootPath = rootPath;
        this.volumeId = volumeId;
        this.defaultTextFileEncoding = defaultTextFileEncoding;
    }

    public String getRootPath() {
        return rootPath;
    }

    public String getVolumeId() {
        return volumeId;
    }

    public String getDefaultTextFileEncoding() {
        return defaultTextFileEncoding;
    }
}
