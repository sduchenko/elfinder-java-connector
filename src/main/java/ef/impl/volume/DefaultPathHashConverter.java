package ef.impl.volume;

import ef.api.*;
import ef.api.Error;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * User: sduchenko
 * Date: 18.04.13
 * Time: 13:37
 */
public class DefaultPathHashConverter implements PathHashConverter {
    private final String volumeId;
    private final String rootPath;

    public DefaultPathHashConverter(String volumeId, String rootPath) {
        this.volumeId = volumeId + "_";
        this.rootPath = rootPath;
    }

    @Override
    public String hashToPath(String hash) {
        if(!hash.startsWith(volumeId)){
            throw new ErrException(Error.errInvName);
        }

        hash = hash.substring(volumeId.length());
        hash = hash.replaceAll("-","+").replaceAll("___","/").replaceAll("_","=");
        String path;
        try {
            path = base64Decode(hash);
        } catch (IOException e) {
            throw new ErrException(e, Error.errInvName);
        }

        return rootPath + path;
    }

    @Override
    public String pathToHash(String path) {
        if(!path.startsWith(rootPath)){
            throw new ErrException(Error.errInvName);
        }

        path = path.replace(rootPath, "");
        path = base64Encode(path);
        path = path.replaceAll("\\=","_").replaceAll("\\+","-").replaceAll("/","___");
        return volumeId + path;
    }

    public static String getVolume(String hash){
        int pos = hash.indexOf("_");
        return pos > 0 ? hash.substring(0, pos) : hash;
    }

    private static final Charset UTF8 = Charset.forName("UTF-8");
    private static final BASE64Encoder ENCODER = new BASE64Encoder();
    private static final BASE64Decoder DECODER = new BASE64Decoder();

    private String base64Encode(String value){
        return ENCODER.encode(value.getBytes(UTF8));
    }

    private String base64Decode(String value) throws IOException {
        return new String(DECODER.decodeBuffer(value), UTF8);
    }
}
