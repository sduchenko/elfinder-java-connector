package ef.impl.volume;

import ef.api.FileEntry;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * User: sduchenko
 * Date: 18.04.13
 * Time: 18:03
 */
public class SimpleFileEntry implements FileEntry {
    private long ts;
    private String hash;
    private String phash;
    private String mime;
    private String name;

    @JsonSerialize(using = BooleanAsNumberSerializer.class)
    private boolean locked;

    @JsonSerialize(using = BooleanAsNumberSerializer.class)
    private boolean read;

    @JsonSerialize(using = BooleanAsNumberSerializer.class)
    private boolean write;

    private long size;
    private String volumeId;

    @JsonSerialize(using = BooleanAsNumberSerializer.class)
    private boolean dirs;

    public void setTs(long ts) {
        this.ts = ts / 1000;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setPhash(String phash) {
        this.phash = phash;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public void setWrite(boolean write) {
        this.write = write;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public void setVolumeId(String volumeId) {
        this.volumeId = volumeId;
    }

    public void setDirs(boolean dirs) {
        this.dirs = dirs;
    }

    @Override
    public long getTs() {
        return ts;
    }

    @Override
    public String getHash() {
        return hash;
    }

    @Override
    public String getPhash() {
        return phash;
    }

    @Override
    public String getMime() {
        return mime;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isLocked() {
        return locked;
    }

    @Override
    public boolean isRead() {
        return read;
    }

    @Override
    public boolean isWrite() {
        return write;
    }

    @Override
    public boolean isDirs() {
        return dirs;
    }

    @Override
    public long getSize() {
        return size;
    }

    @Override
    public String getVolumeId() {
        return volumeId;
    }

    @Override
    public String toString() {
        return "SimpleFileEntry{" +
                "ts=" + ts +
                ", hash='" + hash + '\'' +
                ", phash='" + phash + '\'' +
                ", mime='" + mime + '\'' +
                ", name='" + name + '\'' +
                ", locked=" + locked +
                ", read=" + read +
                ", write=" + write +
                ", size=" + size +
                ", volumeId='" + volumeId + '\'' +
                ", dirs=" + dirs +
                '}';
    }
}
