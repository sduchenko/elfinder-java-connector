package ef.impl.volume;

import ef.api.FileData;
import ef.api.FileEntry;

import java.io.InputStream;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 13:38
 */
public class SimpleFileData implements FileData {

    private final FileEntry entry;
    private final InputStream is;

    public SimpleFileData(FileEntry entry, InputStream is) {
        this.entry = entry;
        this.is = is;
    }

    @Override
    public FileEntry getEntry() {
        return entry;
    }

    @Override
    public InputStream getInputStream() {
        return is;
    }
}
