package ef.impl.volume;

import ef.api.*;
import ef.api.Error;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: sduchenko
 * Date: 19.04.13
 * Time: 14:42
 */
public class VolumeManagerImpl implements VolumeManager {
    private volatile Map<String, Volume> volumes = new HashMap<String, Volume>();
    private AtomicInteger volumeCounter = new AtomicInteger();

    @Override
    public Volume getVolume(String hash) {
        String volumeId = DefaultPathHashConverter.getVolume(hash);
        return volumes.get(volumeId);
    }

    @Override
    public Volume getRequiredVolume(String hash) {
        Volume volume = getVolume(hash);
        if(volume == null){
            throw new ErrException(Error.errFolderNotFound);
        }
        return volume;
    }

    @Override
    public synchronized Volume createFsVolume(String rootPath, String defaultTextFileEncoding) {
        File path = new File(rootPath);
        try {
            rootPath = path.getCanonicalPath();
        } catch (IOException e) {
            throw new RuntimeException("unable to create volume", e);
        }
        String volumeId = getNewVolumeId();
        Volume volume = new FsVolume(
                new DefaultPathHashConverter(volumeId, rootPath),
                new FsConfig(rootPath, volumeId, defaultTextFileEncoding)
        );
        volumes.put(volumeId, volume);
        return volume;
    }

    @Override
    public Collection<Volume> getVolumes() {
        return Collections.unmodifiableCollection(volumes.values());
    }

    private String getNewVolumeId(){
        return "v" + volumeCounter.incrementAndGet();
    }
}
