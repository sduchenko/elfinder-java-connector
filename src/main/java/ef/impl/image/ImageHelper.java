package ef.impl.image;


import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * User: sduchenko
 * Date: 24.04.13
 * Time: 17:16
 */
public class ImageHelper {
    public static void resize(File img, long newWidth, long newHeight) throws IOException {
        BufferedImage image = ImageIO.read(img);
        BufferedImage newImage = Scalr.resize(image, Scalr.Method.QUALITY, Scalr.Mode.FIT_EXACT, (int)newWidth, (int)newHeight);
        ImageIO.write(newImage, FilenameUtils.getExtension(img.getName()), img);
    }

    public static void rotate(File img, long degree) throws IOException {
        BufferedImage image = ImageIO.read(img);
        BufferedImage newImage = Scalr.rotate(image, Scalr.Rotation.CW_90);
        ImageIO.write(newImage, FilenameUtils.getExtension(img.getName()), img);
    }

    public static void crop(File img, long newWidth, long newHeight, long x, long y) throws IOException {
        BufferedImage image = ImageIO.read(img);
        BufferedImage newImage = Scalr.crop(image, (int) x, (int) y, (int) newWidth, (int) newHeight);
        ImageIO.write(newImage, FilenameUtils.getExtension(img.getName()), img);
    }
}
