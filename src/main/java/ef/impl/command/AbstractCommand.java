package ef.impl.command;

import ef.api.Command;
import ef.api.Request;
import ef.api.VolumeManager;

/**
 * User: sduchenko
 * Date: 19.04.13
 * Time: 15:04
 */
public abstract class AbstractCommand<T extends Request> implements Command<T> {
    protected final VolumeManager volumeManager;

    public AbstractCommand(VolumeManager volumeManager) {
        this.volumeManager = volumeManager;
    }
}
