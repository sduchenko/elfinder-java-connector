package ef.impl.command.rename;

import ef.api.Request;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 14:58
 */
public class RenameRequest implements Request {
    String target;
    String name;
}
