package ef.impl.command.rename;

import ef.api.*;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

import java.util.Arrays;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 14:58
 */
public class RenameCommand extends AbstractCommand<RenameRequest> {

    public RenameCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public RenameRequest buildRequest(SourceRequestReader requestReader) {
        RenameRequest request = new RenameRequest();
        request.target = requestReader.getRequiredString("target");
        request.name = requestReader.getRequiredString("name");
        return request;
    }

    @Override
    public Response execute(RenameRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.target);
        FileEntry entry = volume.rename(request.target, request.name);
        MapResponse response = new MapResponse();
        response.put("removed", Arrays.asList(request.target));
        response.put("added", Arrays.asList(entry));
        return response;
    }
}
