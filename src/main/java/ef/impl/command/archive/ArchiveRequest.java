package ef.impl.command.archive;

import ef.api.Request;

/**
 * User: sduchenko
 * Date: 24.04.13
 * Time: 10:57
 */
public class ArchiveRequest implements Request {
    String type;
    String[] targets;
}
