package ef.impl.command.archive;

import ef.api.*;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

import java.util.Arrays;

/**
 * User: sduchenko
 * Date: 24.04.13
 * Time: 10:58
 */
public class ArchiveCommand extends AbstractCommand<ArchiveRequest> {
    public ArchiveCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public ArchiveRequest buildRequest(SourceRequestReader requestReader) {
        ArchiveRequest request = new ArchiveRequest();
        request.type = requestReader.getRequiredString("type");
        request.targets = requestReader.getRequiredStringArray("targets[]");
        return request;
    }

    @Override
    public Response execute(ArchiveRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.targets[0]);
        FileEntry entry = volume.archive(request.type, request.targets);
        MapResponse response = new MapResponse();
        response.put("added", Arrays.asList(entry));
        return response;
    }
}
