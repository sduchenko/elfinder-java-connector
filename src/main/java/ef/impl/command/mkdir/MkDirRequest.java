package ef.impl.command.mkdir;

import ef.api.Request;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 14:47
 */
public class MkDirRequest implements Request {
    String target;
    String name;
}
