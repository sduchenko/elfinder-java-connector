package ef.impl.command.mkdir;

import ef.api.*;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

import java.util.Arrays;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 14:47
 */
public class MkDirCommand extends AbstractCommand<MkDirRequest> {

    public MkDirCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public MkDirRequest buildRequest(SourceRequestReader requestReader) {
        MkDirRequest request = new MkDirRequest();
        request.target = requestReader.getRequiredString("target");
        request.name = requestReader.getRequiredString("name");
        return request;
    }

    @Override
    public Response execute(MkDirRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.target);
        FileEntry entry = volume.mkDir(request.target, request.name);
        MapResponse response = new MapResponse();
        response.put("added", Arrays.asList(entry));
        return response;
    }
}
