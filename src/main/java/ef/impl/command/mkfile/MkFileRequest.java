package ef.impl.command.mkfile;

import ef.api.Request;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 14:52
 */
public class MkFileRequest implements Request {
    String target;
    String name;
}
