package ef.impl.command.mkfile;

import ef.api.*;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

import java.util.Arrays;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 14:53
 */
public class MkFileCommand extends AbstractCommand<MkFileRequest> {

    public MkFileCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public MkFileRequest buildRequest(SourceRequestReader requestReader) {
        MkFileRequest request = new MkFileRequest();
        request.target = requestReader.getRequiredString("target");
        request.name = requestReader.getRequiredString("name");
        return request;
    }

    @Override
    public Response execute(MkFileRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.target);
        FileEntry entry = volume.mkFile(request.target, request.name);
        MapResponse response = new MapResponse();
        response.put("added", Arrays.asList(entry));
        return response;
    }
}
