package ef.impl.command.rm;

import ef.api.Response;
import ef.api.SourceRequestReader;
import ef.api.Volume;
import ef.api.VolumeManager;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 16:15
 */
public class RmCommand extends AbstractCommand<RmRequest> {
    public RmCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public RmRequest buildRequest(SourceRequestReader requestReader) {
        RmRequest request = new RmRequest();
        request.targets = requestReader.getRequiredStringArray("targets[]");
        return request;
    }

    @Override
    public Response execute(RmRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.targets[0]);

        for(String target: request.targets){
            volume.remove(target);
        }

        MapResponse response = new MapResponse();
        response.put("removed", request.targets);
        return response;
    }
}
