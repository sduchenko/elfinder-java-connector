package ef.impl.command.duplicate;

import ef.api.*;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 16:20
 */
public class DuplicateCommand extends AbstractCommand<DuplicateRequest> {
    public DuplicateCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public DuplicateRequest buildRequest(SourceRequestReader requestReader) {
        DuplicateRequest request = new DuplicateRequest();
        request.targets = requestReader.getRequiredStringArray("targets[]");
        return request;
    }

    @Override
    public Response execute(DuplicateRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.targets[0]);
        List<FileEntry> added = new ArrayList<FileEntry>();

        for(String target: request.targets){
            added.add(volume.duplicate(target));
        }

        MapResponse response = new MapResponse();
        response.put("added", added);
        return response;
    }
}
