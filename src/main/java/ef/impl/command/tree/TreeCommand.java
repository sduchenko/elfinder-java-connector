package ef.impl.command.tree;

import ef.api.*;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

import java.util.List;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 14:23
 */
public class TreeCommand extends AbstractCommand<TreeRequest> {

    public TreeCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public TreeRequest buildRequest(SourceRequestReader requestReader) {
        TreeRequest request = new TreeRequest();
        request.target = requestReader.getRequiredString("target");
        return request;
    }

    @Override
    public Response execute(TreeRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.target);
        List<FileEntry> list = volume.tree (request.target);
        MapResponse response = new MapResponse();
        response.put("tree", list);
        return response;
    }
}
