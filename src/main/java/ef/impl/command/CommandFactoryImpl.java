package ef.impl.command;

import ef.api.*;
import ef.api.Error;
import ef.impl.command.archive.ArchiveCommand;
import ef.impl.command.duplicate.DuplicateCommand;
import ef.impl.command.extract.ExtractCommand;
import ef.impl.command.file.FileCommand;
import ef.impl.command.get.GetCommand;
import ef.impl.command.mkdir.MkDirCommand;
import ef.impl.command.mkfile.MkFileCommand;
import ef.impl.command.open.OpenCommand;
import ef.impl.command.parents.ParentsCommand;
import ef.impl.command.paste.PasteCommand;
import ef.impl.command.ping.PingCommand;
import ef.impl.command.put.PutCommand;
import ef.impl.command.rename.RenameCommand;
import ef.impl.command.resize.ResizeCommand;
import ef.impl.command.rm.RmCommand;
import ef.impl.command.tree.TreeCommand;
import ef.impl.command.upload.UploadCommand;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 11:13
 */
public class CommandFactoryImpl implements CommandFactory {

    private volatile Map<String, Command> commands = new HashMap<String, Command>();
    private final VolumeManager volumeManager;

    public CommandFactoryImpl(VolumeManager volumeManager) {
        this.volumeManager = volumeManager;
        register("open", OpenCommand.class);
        register("file", FileCommand.class);
        register("get", GetCommand.class);
        register("parents", ParentsCommand.class);
        register("tree", TreeCommand.class);
        register("mkdir", MkDirCommand.class);
        register("mkfile", MkFileCommand.class);
        register("rename", RenameCommand.class);
        register("ping", PingCommand.class);
        register("paste", PasteCommand.class);
        register("rm", RmCommand.class);
        register("duplicate", DuplicateCommand.class);
        register("put", PutCommand.class);
        register("upload", UploadCommand.class);
        register("archive", ArchiveCommand.class);
        register("extract", ExtractCommand.class);
        register("resize", ResizeCommand.class);
    }

    @Override
    public Command getCommand(String cmd) {
        Command command = commands.get(cmd);
        if(command == null){
            throw new ErrException(Error.errCmdNoSupport, cmd);
        }
        return command;
    }

    private void register(String cmd, Class<? extends AbstractCommand> clazz){
        AbstractCommand command;
        try {
            Constructor<? extends AbstractCommand> constructor = clazz.getConstructor(VolumeManager.class);
            command = constructor.newInstance(volumeManager);
        } catch (Exception e) {
            throw new RuntimeException("unable to register command handler: "+cmd, e);
        }
        commands.put(cmd, command);
    }
}
