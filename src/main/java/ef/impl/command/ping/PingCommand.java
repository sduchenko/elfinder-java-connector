package ef.impl.command.ping;

import ef.api.Response;
import ef.api.SourceRequestReader;
import ef.api.VolumeManager;
import ef.impl.command.AbstractCommand;
import ef.impl.response.WritingResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 15:48
 */
public class PingCommand extends AbstractCommand<PingRequest> {
    public PingCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public PingRequest buildRequest(SourceRequestReader requestReader) {
        return new PingRequest();
    }

    @Override
    public Response execute(PingRequest request) {
        return RESPONSE;
    }

    private final Response RESPONSE = new WritingResponse() {
        @Override
        public void write(HttpServletResponse resp) throws IOException {
            resp.addHeader("Connection", "close");
            resp.getWriter().print("");
        }
    };
}
