package ef.impl.command.get;

import ef.api.Response;
import ef.api.SourceRequestReader;
import ef.api.Volume;
import ef.api.VolumeManager;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 13:57
 */
public class GetCommand extends AbstractCommand<GetRequest> {

    public GetCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public GetRequest buildRequest(SourceRequestReader requestReader) {
        GetRequest request = new GetRequest();
        request.target = requestReader.getRequiredString("target");
        return request;
    }

    @Override
    public Response execute(GetRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.target);
        String content = volume.readText(request.target);
        MapResponse response = new MapResponse();
        response.put("content", content);
        return response;
    }
}
