package ef.impl.command.extract;

import ef.api.*;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

import java.util.Arrays;

/**
 * User: sduchenko
 * Date: 24.04.13
 * Time: 13:16
 */
public class ExtractCommand extends AbstractCommand<ExtractRequest> {
    public ExtractCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public ExtractRequest buildRequest(SourceRequestReader requestReader) {
        ExtractRequest request = new ExtractRequest();
        request.target = requestReader.getRequiredString("target");
        return request;
    }

    @Override
    public Response execute(ExtractRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.target);
        FileEntry entry = volume.extract(request.target);
        MapResponse response = new MapResponse();
        response.put("added", Arrays.asList(entry));
        return response;
    }
}
