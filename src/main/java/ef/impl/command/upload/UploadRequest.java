package ef.impl.command.upload;

import ef.api.Request;
import org.apache.commons.fileupload.FileItem;

import java.util.List;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 17:18
 */
public class UploadRequest implements Request {
    String target;
    List<FileItem> upload;
}
