package ef.impl.command.open;

import ef.api.*;
import ef.api.Error;
import ef.impl.archive.ArchiveType;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * User: sduchenko
 * Date: 19.04.13
 * Time: 14:58
 */
public class OpenCommand extends AbstractCommand<OpenRequest> {

    public OpenCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public OpenRequest buildRequest(SourceRequestReader requestReader) {
        OpenRequest request = new OpenRequest();
        request.init = requestReader.getOptionalBoolean("init", false);
        request.tree = requestReader.getOptionalBoolean("tree", false);
        if(request.init){
            request.target = requestReader.getOptionalString("target", null);
        }else{
            request.target = requestReader.getRequiredString("target");
        }
        return request;
    }

    @Override
    public Response execute(OpenRequest request) {
        List<FileEntry> list;
        FileEntry cwd = null;

        if(request.init && StringUtils.isEmpty(request.target)){
            list = new ArrayList<FileEntry>();
            Collection<Volume> volumes = volumeManager.getVolumes();
            Volume cwdVolume = null;
            for(Volume volume: volumes){
                FileEntry entry = volume.root();
                if(entry.isRead() && cwd == null){
                    cwd = entry;
                    cwdVolume = volume;
                }
                list.add(entry);
            }

            if(cwdVolume == null){
                throw new ErrException(Error.errNoVolumes);
            }

            if(request.tree){
                list.addAll(cwdVolume.list(cwd.getHash()));
            }
        }else{
            Volume volume = volumeManager.getRequiredVolume(request.target);
            cwd = volume.get(request.target);
            list = volume.list(request.target);
        }

        MapResponse response = new MapResponse();

        if(request.init){
            response.put("api", 2);
            response.put("options", OPTIONS);
        }

        response.put("files", list);

        if(cwd != null){
            response.put("cwd", cwd);
        }

        return response;
    }

    private static Map<String, Object> OPTIONS;
    static {
        OPTIONS = new HashMap<String, Object>();
        Map<String, Object> archivers = new HashMap<String, Object>();
        archivers.put("create", ArchiveType.getMimeTypes());
        archivers.put("extract", ArchiveType.getMimeTypes());
        OPTIONS.put("archivers", archivers);
        OPTIONS.put("disabled", Arrays.asList("tmb","netmout", "ls"));
    }
}
