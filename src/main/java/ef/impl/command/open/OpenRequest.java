package ef.impl.command.open;

import ef.api.Request;

/**
 * User: sduchenko
 * Date: 19.04.13
 * Time: 14:58
 */
public class OpenRequest implements Request {
    boolean init;
    boolean tree;
    String target;
}
