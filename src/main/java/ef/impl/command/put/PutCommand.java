package ef.impl.command.put;

import ef.api.*;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 17:07
 */
public class PutCommand extends AbstractCommand<PutRequest> {
    public PutCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public PutRequest buildRequest(SourceRequestReader requestReader) {
        PutRequest request = new PutRequest();
        request.target = requestReader.getRequiredString("target");
        request.content = requestReader.getRequiredString("content");
        return request;
    }

    @Override
    public Response execute(PutRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.target);
        volume.writeText(request.target, request.content);
        FileEntry file = volume.get(request.target);
        MapResponse response = new MapResponse();
        response.put("file", file);
        return response;
    }
}
