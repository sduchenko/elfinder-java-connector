package ef.impl.command.put;

import ef.api.Request;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 17:07
 */
public class PutRequest implements Request {
    String target;
    String content;
}
