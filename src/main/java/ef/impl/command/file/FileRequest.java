package ef.impl.command.file;

import ef.api.Request;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 13:28
 */
public class FileRequest implements Request {
    String target;
    boolean download;
}
