package ef.impl.command.file;

import ef.api.*;
import ef.impl.command.AbstractCommand;
import ef.impl.response.HeaderUtil;
import ef.impl.response.WritingResponse;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 13:28
 */
public class FileCommand extends AbstractCommand<FileRequest> {

    public FileCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public FileRequest buildRequest(SourceRequestReader requestReader) {
        FileRequest request = new FileRequest();
        request.target = requestReader.getRequiredString("target");
        request.download = requestReader.getOptionalBoolean("download", false);
        return request;
    }

    @Override
    public Response execute(final FileRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.target);
        final FileData data = volume.read(request.target);

        return new WritingResponse() {
            @Override
            public void write(HttpServletResponse resp) throws IOException{
                try {
                    if(request.download){
                        HeaderUtil.writeDisposition(resp, data.getEntry().getName());
                    }else{
                        resp.setContentType(data.getEntry().getMime());
                    }

                    IOUtils.copy(data.getInputStream(), resp.getOutputStream());
                } finally {
                    data.getInputStream().close();
                }
            }
        };
    }
}
