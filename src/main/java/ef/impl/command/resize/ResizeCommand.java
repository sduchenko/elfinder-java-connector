package ef.impl.command.resize;

import ef.api.*;
import ef.api.Error;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

import java.util.Arrays;

/**
 * User: sduchenko
 * Date: 24.04.13
 * Time: 16:59
 */
public class ResizeCommand extends AbstractCommand<ResizeRequest> {
    public ResizeCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public ResizeRequest buildRequest(SourceRequestReader requestReader) {
        ResizeRequest request = new ResizeRequest();
        request.target = requestReader.getRequiredString("target");
        String mode = requestReader.getRequiredString("mode");

        try {
            request.mode = ResizeMode.valueOf(mode.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new ErrException(Error.errCmdParams, "invalid mode");
        }
        request.width = requestReader.getRequiredLong("width");
        request.height = requestReader.getRequiredLong("height");

        if(request.mode == ResizeMode.ROTATE){
            request.degree = requestReader.getRequiredLong("degree");
        }

        if(request.mode == ResizeMode.CROP){
            request.x = requestReader.getRequiredLong("x");
            request.y = requestReader.getRequiredLong("y");
        }

        return request;
    }

    @Override
    public Response execute(ResizeRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.target);

        FileEntry entry;
        switch (request.mode){
            case RESIZE: entry = volume.resizeImage(request.target, request.width, request.height); break;
            case ROTATE: entry = volume.rotateImage(request.target, request.width, request.height, request.degree); break;
            case CROP: entry = volume.cropImage(request.target, request.width, request.height, request.x, request.y); break;
            default:
                throw new ErrException(Error.errUnknown);
        }

        MapResponse response = new MapResponse();
        response.put("changed", Arrays.asList(entry));
        return response;
    }
}
