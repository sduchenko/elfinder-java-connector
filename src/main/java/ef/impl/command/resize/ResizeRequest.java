package ef.impl.command.resize;

import ef.api.Request;

/**
 * User: sduchenko
 * Date: 24.04.13
 * Time: 16:56
 */
public class ResizeRequest implements Request {
    String target;
    long width;
    long height;
    long x;
    long y;
    long degree;
    ResizeMode mode;
}
