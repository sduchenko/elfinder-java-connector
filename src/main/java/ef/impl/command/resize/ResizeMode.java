package ef.impl.command.resize;

/**
 * User: sduchenko
 * Date: 24.04.13
 * Time: 16:57
 */
public enum ResizeMode {
    CROP, ROTATE, RESIZE;
}
