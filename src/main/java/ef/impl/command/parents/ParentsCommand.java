package ef.impl.command.parents;

import ef.api.*;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

import java.util.List;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 14:04
 */
public class ParentsCommand extends AbstractCommand<ParentsRequest> {

    public ParentsCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public ParentsRequest buildRequest(SourceRequestReader requestReader) {
        ParentsRequest request = new ParentsRequest();
        request.target = requestReader.getRequiredString("target");
        return request;
    }

    @Override
    public Response execute(ParentsRequest request) {
        Volume volume = volumeManager.getRequiredVolume(request.target);
        List<FileEntry> list = volume.parents(request.target);
        MapResponse response = new MapResponse();

        for(Volume vol: volumeManager.getVolumes()){
            list.add(vol.root());
        }

        response.put("tree", list);
        return response;
    }
}
