package ef.impl.command.paste;

import ef.api.*;
import ef.api.Error;
import ef.impl.command.AbstractCommand;
import ef.impl.response.MapResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 15:54
 */
public class PasteCommand extends AbstractCommand<PasteRequest> {

    public PasteCommand(VolumeManager volumeManager) {
        super(volumeManager);
    }

    @Override
    public PasteRequest buildRequest(SourceRequestReader requestReader) {
        PasteRequest request = new PasteRequest();
        request.src = requestReader.getRequiredString("src");
        request.dst = requestReader.getRequiredString("dst");
        request.cut = requestReader.getOptionalBoolean("cut", false);
        request.targets = requestReader.getRequiredStringArray("targets[]");
        return request;
    }

    @Override
    public Response execute(PasteRequest request) {
        if(request.src.equals(request.dst)){
            throw new ErrException(Error.errCopyInItself);
        }

        Volume volume = volumeManager.getRequiredVolume(request.src);
        Volume dstVolume = volumeManager.getRequiredVolume(request.dst);

        if(volume != dstVolume){
            throw new ErrException(Error.errTrgFolderNotFound);
        }

        List<FileEntry> added = new ArrayList<FileEntry>();
        MapResponse response = new MapResponse();

        if(request.cut){
            List<String> removed = new ArrayList<String>();
            for(String target: request.targets){
                added.add(volume.move(target, request.dst));
                removed.add(target);
            }
            response.put("removed", removed);
        }else{
            for(String target: request.targets){
                added.add(volume.copy(target, request.dst));
            }
        }

        response.put("added", added);
        return response;
    }
}
