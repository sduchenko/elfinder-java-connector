package ef.impl.command.paste;

import ef.api.Request;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 15:52
 */
public class PasteRequest implements Request {
    String src;
    String dst;
    String[] targets;
    boolean cut;
}
