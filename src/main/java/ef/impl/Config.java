package ef.impl;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Date: 26.04.13
 * Time: 19:58
 */
public class Config {
    private static final String PROPS_FILE = "elfinder.properties";
    private Configuration config;

    public Config() {
        PropertiesConfiguration config = new PropertiesConfiguration();
        config.setFileName(getClass().getClassLoader().getResource(PROPS_FILE).getFile());
        config.setEncoding("UTF-8");
        try {
            config.load();
        } catch (ConfigurationException e) {
            throw new RuntimeException(e);
        }
        this.config = config;
    }

    public List<Mount> getMountList(){
        List<Mount> mounts = new ArrayList<Mount>();
        for(int i=1;i<10;i++){
            String baseName = "mount."+i;
            String type = config.getString(baseName);
            if(StringUtils.isEmpty(type)){
                break;
            }

            mounts.add(new Mount(baseName, type));
        }
        return mounts;
    }

    public int getUploadMaxSize(){
        return config.getInt("upload.max.size", 10000000);
    }

    public class Mount {
        private final String baseName;
        private final String type;

        Mount(String baseName, String type) {
            this.baseName = baseName;
            this.type = type.toLowerCase();
        }

        public String getRequiredParam(String paramName){
            String value = getParam(paramName, null);
            if(StringUtils.isEmpty(value)){
                throw new RuntimeException("param '"+paramName+"' not found for mount: "+baseName);
            }
            return value;
        }

        public String getParam(String paramName, String defaultValue){
            String value = config.getString(baseName+"."+paramName);
            return value == null ? defaultValue : value;
        }

        public String getBaseName() {
            return baseName;
        }

        public String getType() {
            return type;
        }

        @Override
        public String toString() {
            return "Mount{" +
                    "baseName='" + baseName + '\'' +
                    ", type='" + type + '\'' +
                    '}';
        }
    }
}
