package ef.impl.response;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 13:46
 */
public abstract class HeaderUtil {
    private HeaderUtil() {
    }

    public static void writeNoCache(HttpServletResponse resp){
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Cache-Control", "no-cache; must-revalidate");
        resp.setHeader("Expires", "-1");
        resp.setDateHeader("Last-Modified", System.currentTimeMillis());
    }

    public static void writeDisposition(HttpServletResponse resp, String fileName){
        resp.setContentType("application/octet-stream");
        try {
            resp.setHeader("Content-Disposition", "inline; filename=" + URLEncoder.encode(fileName,"UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
