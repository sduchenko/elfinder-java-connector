package ef.impl.response;

import ef.api.Response;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 13:32
 */
public abstract class WritingResponse implements Response {
    public abstract void write(HttpServletResponse resp) throws IOException;
}
