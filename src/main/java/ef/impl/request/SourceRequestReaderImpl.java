package ef.impl.request;

import ef.api.ErrException;
import ef.api.Error;
import ef.api.SourceRequestReader;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 10:58
 */
public class SourceRequestReaderImpl implements SourceRequestReader {

    private final HttpServletRequest request;
    private final ServletFileUpload fileUpload;
    private Object parsedUpload;

    public SourceRequestReaderImpl(HttpServletRequest request, ServletFileUpload fileUpload) {
        this.request = request;
        this.fileUpload = fileUpload;
    }

    @Override
    public String getRequiredString(String paramName) {
        String param = request.getParameter(paramName);
        if(StringUtils.isEmpty(param) || param.equalsIgnoreCase("null")){
            throw new ErrException(Error.errCmdParams, paramName);
        }
        return param;
    }

    @Override
    public String getOptionalString(String paramName, String defaultValue) {
        String param = request.getParameter(paramName);
        return StringUtils.isEmpty(param) || param.equalsIgnoreCase("null") ? defaultValue : param;
    }

    @Override
    public String[] getRequiredStringArray(String paramName) {
        String[] values = request.getParameterValues(paramName);
        if(values == null || values.length == 0){
            throw new ErrException(Error.errCmdParams, paramName);
        }
        return values;
    }

    @Override
    public long getRequiredLong(String paramName) {
        String param = getRequiredString(paramName);
        try {
            return Long.valueOf(param);
        } catch (NumberFormatException e) {
            throw new ErrException(Error.errCmdParams, paramName);
        }
    }

    @Override
    public long getOptionalLong(String paramName, long defaultValue) {
        String param = request.getParameter(paramName);
        if(param == null){
            return defaultValue;
        }

        try {
            return Long.valueOf(param);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    @Override
    public boolean getRequiredBoolean(String paramName) {
        String param = getRequiredString(paramName);
        return param.equals("1");
    }

    @Override
    public boolean getOptionalBoolean(String paramName, boolean defaultValue) {
        String param = request.getParameter(paramName);
        if(param == null){
            return defaultValue;
        }
        return param.equals("1");
    }

    @Override
    public boolean isUpload() {
        return ServletFileUpload.isMultipartContent(request);
    }

    @Override
    public Map<String, List<FileItem>> getFileUpload() {
        if(parsedUpload != null){
            if(parsedUpload instanceof ErrException){
                throw (ErrException) parsedUpload;
            }

            return (Map<String, List<FileItem>>) parsedUpload;
        }

        try {
            parsedUpload = fileUpload.parseParameterMap(request);
            return (Map<String, List<FileItem>>) parsedUpload;
        } catch (FileUploadException e) {
            if(e instanceof FileUploadBase.FileSizeLimitExceededException){
                parsedUpload = new ErrException(e, Error.errFileMaxSize, "Allowed size is "+fileUpload.getFileSizeMax() / 1024 + " Kb");
            }else
            if(e instanceof FileUploadBase.InvalidContentTypeException){
                parsedUpload =  new ErrException(e, Error.errUploadMime);
            }else
            if(e instanceof FileUploadBase.IOFileUploadException){
                parsedUpload =  new ErrException(e, Error.errUploadTransfer);
            }else{
                parsedUpload =  new ErrException(e, Error.errUploadCommon);
            }

            throw (ErrException) parsedUpload;
        }
    }

    @Override
    public String getRequiredUploadString(String paramName) {
        List<FileItem> items = getFileUpload().get(paramName);
        if(items == null || items.isEmpty()){
            throw new ErrException(Error.errCmdParams, paramName);
        }

        FileItem item = items.get(0);
        if(!item.isFormField()){
            throw new ErrException(Error.errCmdParams, paramName);
        }
        return item.getString();
    }
}
