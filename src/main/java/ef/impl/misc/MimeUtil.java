package ef.impl.misc;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 13:09
 */
public abstract class MimeUtil {

    private MimeUtil() {
    }

    private static final String DIR = "directory";
    private static final javax.activation.MimetypesFileTypeMap MAP;

    static {
        MAP = new javax.activation.MimetypesFileTypeMap();
        String filePath = MimeUtil.class.getClassLoader().getResource("mime.types").getFile();
        try {
            String content = FileUtils.readFileToString(new File(filePath));
            MAP.addMimeTypes(content);
        } catch (IOException e) {
            throw new RuntimeException("unable to read mime.types file", e);
        }
    }
    public static String getMime(File file){
        if(file.isDirectory()){
            return DIR;
        }

        return MAP.getContentType(file);
    }
}
