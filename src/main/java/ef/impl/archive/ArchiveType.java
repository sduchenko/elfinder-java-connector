package ef.impl.archive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: sduchenko
 * Date: 23.04.13
 * Time: 18:45
 */
public enum ArchiveType {
    TAR("application/x-tar", "tar"), ZIP("application/zip", "zip");

    private final String mimeType;
    private final String fileExt;
    private ArchiveType(String mimeType, String fileExt) {
        this.mimeType = mimeType;
        this.fileExt = fileExt;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getFileExt() {
        return fileExt;
    }

    public static ArchiveType fromMime(String mimeType){
        for(ArchiveType type: values()){
            if(type.getMimeType().equalsIgnoreCase(mimeType)){
                return type;
            }
        }
        throw new IllegalArgumentException("unsupported mime type");
    }

    public static ArchiveType fromFileExt(String fileExt){
        for(ArchiveType type: values()){
            if(type.getFileExt().equalsIgnoreCase(fileExt)){
                return type;
            }
        }
        throw new IllegalArgumentException("unsupported mime type");
    }

    private static final List<String> mimeTypes;
    static {
        List<String> list = new ArrayList<String>();

        for(ArchiveType type: values()){
            list.add(type.getMimeType());
        }

        mimeTypes = Collections.unmodifiableList(list);
    }

    public static List<String> getMimeTypes(){
        return mimeTypes;
    }
}
