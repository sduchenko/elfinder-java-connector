package ef.api;

import org.apache.commons.fileupload.FileItem;

import java.util.List;
import java.util.Map;

/**
 * User: sduchenko
 * Date: 18.04.13
 * Time: 13:08
 */
public interface SourceRequestReader {
    String getRequiredString(String paramName);
    String getOptionalString(String paramName, String defaultValue);
    String[] getRequiredStringArray(String paramName);
    long getRequiredLong(String paramName);
    long getOptionalLong(String paramName, long defaultValue);
    boolean getRequiredBoolean(String paramName);
    boolean getOptionalBoolean(String paramName, boolean defaultValue);
    boolean isUpload();
    Map<String, List<FileItem>> getFileUpload();
    String getRequiredUploadString(String paramName);
}
