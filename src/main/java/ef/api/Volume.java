package ef.api;

import java.io.InputStream;
import java.util.List;

/**
 * User: sduchenko
 * Date: 18.04.13
 * Time: 13:13
 */
public interface Volume {
    String id();
    FileEntry root();
    FileEntry get(String target);
    String readText(String target);
    FileData read(String target);
    void writeText(String target, String text);
    void remove(String target);
    FileEntry upload(String targetDir, String fileName, InputStream is);
    List<FileEntry> list(String targetDir);
    List<FileEntry> parents(String targetDir);
    List<FileEntry> tree(String targetDir);
    FileEntry mkDir(String targetDir, String dirName);
    FileEntry mkFile(String targetDir, String fileName);
    FileEntry rename(String target, String fileName);
    FileEntry copy(String target, String targetDir);
    FileEntry move(String target, String targetDir);
    FileEntry duplicate(String target);
    FileEntry archive(String mimeType, String[] targets);
    FileEntry extract(String target);
    FileEntry resizeImage(String target, long newWidth, long newHeight);
    FileEntry rotateImage(String target, long newWidth, long newHeight, long degree);
    FileEntry cropImage(String target, long newWidth, long newHeight, long x, long y);
}
