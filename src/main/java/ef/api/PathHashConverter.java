package ef.api;

/**
 * User: sduchenko
 * Date: 18.04.13
 * Time: 13:35
 */
public interface PathHashConverter {
    String hashToPath(String hash);
    String pathToHash(String path);
}
