package ef.api;

import java.util.Collection;

/**
 * User: sduchenko
 * Date: 19.04.13
 * Time: 14:37
 */
public interface VolumeManager {
    Volume getVolume(String hash);
    Volume getRequiredVolume(String hash);
    Volume createFsVolume(String rootPath, String defaultTextFileEncoding);
    Collection<Volume> getVolumes();
}
