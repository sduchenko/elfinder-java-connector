package ef.api;

import org.apache.commons.lang.StringUtils;

/**
 * User: sduchenko
 * Date: 18.04.13
 * Time: 13:17
 */
public class ErrException extends RuntimeException {
    private final Error error;
    private final String parameter;

    public ErrException() {
        this(Error.errUnknown, null);
    }

    public ErrException(Error error) {
        this(error, null);
    }

    public ErrException(Error error, String parameter) {
        this.error = error;
        this.parameter = parameter;
    }

    public ErrException(Throwable cause) {
        this(cause, Error.errUnknown, null);
    }

    public ErrException(Throwable cause, Error error) {
        this(cause, error, null);
    }

    public ErrException(Throwable cause, Error error, String parameter) {
        super(cause);
        this.error = error;
        this.parameter = parameter;
    }

    public Error getError() {
        return error;
    }

    public String getParameter() {
        return parameter;
    }

    public String[] getErrorArray(){
        return StringUtils.isEmpty(parameter) ? new String[]{error.getErrName()} : new String[]{error.getErrName(), parameter};
    }

    @Override
    public String toString() {
        return "ErrException{" +
                "error=" + error +
                ", parameter='" + parameter + '\'' +
                "} " + super.toString();
    }
}
