package ef.api;

/**
 * User: sduchenko
 * Date: 18.04.13
 * Time: 13:17
 */
public enum Error {
    error("error"), // Error
    errUnknown("errUnknown"), // Unknown error.
    errUnknownCmd("errUnknownCmd"), // Unknown command.
    errJqui("errJqui"), // Invalid jQuery UI configuration. Selectable, draggable and droppable components must be included.
    errNode("errNode"), // elFinder requires DOM Element to be created.
    errURL("errURL"), // Invalid elFinder configuration! URL option is not set.
    errAccess("errAccess"), // Access denied.
    errConnect("errConnect"), // Unable to connect to backend.
    errAbort("errAbort"), // Connection aborted.
    errTimeout("errTimeout"), // Connection timeout.
    errNotFound("errNotFound"), // Backend not found.
    errResponse("errResponse"), // Invalid backend response.
    errConf("errConf"), // Invalid backend configuration.
    errJSON("errJSON"), // PHP JSON module not installed.
    errNoVolumes("errNoVolumes"), // Readable volumes not available.
    errCmdParams("errCmdParams"), // Invalid parameters for command "$1".
    errDataNotJSON("errDataNotJSON"), // Data is not JSON.
    errDataEmpty("errDataEmpty"), // Data is empty.
    errCmdReq("errCmdReq"), // Backend request requires command name.
    errOpen("errOpen"), // Unable to open "$1".
    errNotFolder("errNotFolder"), // Object is not a folder.
    errNotFile("errNotFile"), // Object is not a file.
    errRead("errRead"), // Unable to read "$1".
    errWrite("errWrite"), // Unable to write into "$1".
    errPerm("errPerm"), // Permission denied.
    errLocked("errLocked"), // "$1" is locked and can not be renamed, moved or removed.
    errExists("errExists"), // File named "$1" already exists.
    errInvName("errInvName"), // Invalid file name.
    errFolderNotFound("errFolderNotFound"), // Folder not found.
    errFileNotFound("errFileNotFound"), // File not found.
    errTrgFolderNotFound("errTrgFolderNotFound"), // Target folder "$1" not found.
    errPopup("errPopup"), // Browser prevented opening popup window. To open file enable it in browser options.
    errMkdir("errMkdir"), // Unable to create folder "$1".
    errMkfile("errMkfile"), // Unable to create file "$1".
    errRename("errRename"), // Unable to rename "$1".
    errCopyFrom("errCopyFrom"), // Copying files from volume "$1" not allowed.
    errCopyTo("errCopyTo"), // Copying files to volume "$1" not allowed.
    errUploadCommon("errUploadCommon"), // Upload error.
    errUpload("errUpload"), // Unable to upload "$1".
    errUploadNoFiles("errUploadNoFiles"), // No files found for upload.
    errMaxSize("errMaxSize"), // Data exceeds the maximum allowed size.
    errFileMaxSize("errFileMaxSize"), // File exceeds maximum allowed size.
    errUploadMime("errUploadMime"), // File type not allowed.
    errUploadTransfer("errUploadTransfer"), // "$1" transfer error.
    errSave("errSave"), // Unable to save "$1".
    errCopy("errCopy"), // Unable to copy "$1".
    errMove("errMove"), // Unable to move "$1".
    errCopyInItself("errCopyInItself"), // Unable to copy "$1" into itself.
    errRm("errRm"), // Unable to remove "$1".
    errExtract("errExtract"), // Unable to extract files from "$1".
    errArchive("errArchive"), // Unable to create archive.
    errArcType("errArcType"), // Unsupported archive type.
    errNoArchive("errNoArchive"), // File is not archive or has unsupported archive type.
    errCmdNoSupport("errCmdNoSupport"), // Backend does not support this command.
    errReplByChild("errReplByChild"), // The folder “$1” can’t be replaced by an item it contains.
    errArcSymlinks("errArcSymlinks"), // For security reason denied to unpack archives contains symlinks.
    errArcMaxSize("errArcMaxSize"), // Archive files exceeds maximum allowed size.
    errResize("errResize"), // Unable to resize "$1".
    errUsupportType("errUsupportType") // Unsupported file type.
    ;

    private final String errName;
    private Error(String errName) {
        this.errName = errName;
    }

    public String getErrName() {
        return errName;
    }
}
