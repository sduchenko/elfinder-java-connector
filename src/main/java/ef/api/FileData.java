package ef.api;

import java.io.InputStream;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 13:36
 */
public interface FileData {
    FileEntry getEntry();
    InputStream getInputStream();
}
