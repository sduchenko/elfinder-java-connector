package ef.api;

/**
 * User: sduchenko
 * Date: 18.04.13
 * Time: 13:00
 */
public interface FileEntry {
    long getTs();
    String getHash();
    String getPhash();
    String getMime();
    String getName();
    boolean isLocked();
    boolean isRead();
    boolean isWrite();
    boolean isDirs();
    long getSize();
    String getVolumeId();
}
