package ef.api;

/**
 * User: sduchenko
 * Date: 22.04.13
 * Time: 11:13
 */
public interface CommandFactory {
    Command getCommand(String cmd);
}
