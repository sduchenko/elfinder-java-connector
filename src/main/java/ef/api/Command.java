package ef.api;

/**
 * User: sduchenko
 * Date: 18.04.13
 * Time: 13:07
 */
public interface Command<T extends Request> {
    T buildRequest(SourceRequestReader requestReader);
    Response execute(T request);
}
